<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="">
  </a>

  <h1 align="center">DOH-Badge v1.1</h1>

  <p align="center">
    <img src="doh-badge-front-3D.png" alt="Logo" width="300" style=""margin:auto;">
  </p>
  <p><img src="doh-badge-back-3D.png" alt="Logo" width="300" style=""margin:auto;"></p>
</p>

    


## About The Project

This printed board circuit (pcb), the delft open hardware badge, is meant to be used to learn about electronics, technology, hardware and software. It has been designed and developed by the delft open hardware group, to promote open hardware design, collaboration and hacking. It is also designed to travel by post so that anyone can send it around as a geeky present, to run your own workshop, experiment, or whatever cool idea you might have.

### Would you like to learn about hardware design?
[This video shows how the badge was designed, and touches upon fundamental aspects of hardware electronics design](https://drive.google.com/drive/folders/18a-_IFVGkzogmwDuQxF8CceoHXmpsg5X)

## Features
Basically you have a board with an ESP-12E as a brain, that you can connect to your pc using a usb cable. You also have three connectors that you can use to manage sensors or actuators.
- ESP-12E
- USB-2 Type B 
- 3 JST XH female connectors

## Getting Started with using the badge
### Prerequisites
You will need some software or library to compile code into the ESP-12E. You can use arduino to code the badge, but you could also use micropython for instance. 

[Installing ESP8266 Board in Arduino IDE (Windows, Mac OS X, Linux)](https://randomnerdtutorials.com/how-to-install-esp8266-board-arduino-ide/)

### Usage
You can find examples of how to use the badge in the examples folder of this repository.

## Contributing
Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are greatly appreciated.
- Create new examples and submit them in a gitlab issue and upload files.
- You can also submit examples via merge request if you are comfortable with using git.
- Replicate the badge and give us feedback. 

## License
CERN Open Hardware License Version 2 - Strongly Reciprocal

## Contact
Community website: [delftopenhardware.nl](delftopenhardware.nl)

Community chat: [https://t.me/DelftOpenHardware](https://t.me/DelftOpenHardware)

